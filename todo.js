// Elementleri Seç
const form = document.querySelector('#todo-form');
const todoInput = document.querySelector('#todo');
const todoList = document.querySelector('.list-group');
const firstCardbody = document.querySelectorAll('.card-body')[0];
const secondCardbody = document.querySelectorAll('.card-body')[1];
const filter = document.querySelector('#filter');
const clearbutton = document.querySelector('#clear-todos');

// ilk fonksiyon:
eventlisteners();

// Eventleri listen etme
function eventlisteners() { //bütün event listenerler:
    form.addEventListener("submit",addTodo);
    document.addEventListener("DOMContentLoaded",addTodosfromStorage)
    secondCardbody.addEventListener("click",deleteTodo);
    filter.addEventListener("keyup",filterTodos);
    clearbutton.addEventListener("click",clearAllTodos);
}



// Functionlar:
function addTodo(e){
    const newTodo = todoInput.value.trim();
    if (newTodo === "") {

     showAlert("danger","bir şey girmediniz!")
    }else if (todoAlreadyExist(newTodo) === true) {
        // Zaten bu todo var.
        showAlert("danger","bu todo zaten var!")
    }else {
        addTodotoUI(newTodo);
        addTodosLocalStorage(newTodo);
        showAlert("success","başarılı eklendi");
    }
    
    e.preventDefault();
}


function addTodotoUI(newTodo){ // String değeri list item olarak ekleyecek.
        /*
        <li class="list-group-item d-flex justify-content-between">
                                    Todo 1
                                    <a href = "#" class ="delete-item">
                                        <i class = "fa fa-remove"></i>
                                    </a>

                                </li>
        */
    // list item oluşturma
    const listItem = document.createElement("li");
          listItem.className = "list-group-item d-flex justify-content-between";
    // link oluşturma
    const link = document.createElement("a");
          link.href = "#";
          link.className = "delete-item";
          link.innerHTML = `<i class = "fa fa-remove"></i>`;
    // Text Note ekleme (Todoyu yazma)
    listItem.appendChild(document.createTextNode(newTodo));
    // Linki ekleme
    listItem.appendChild(link);
    // Todoliste yeni item'ı ekleme:
    todoList.appendChild(listItem);
    todoInput.value = "";
}


function showAlert(type,message){ // Alert oluşturma
    /*
        <div class="alert alert-danger" role="alert">
        This is a danger alert—check it out!
        </div>
    */
    const alert = document.createElement("div");
    alert.className = `alert alert-${type}`;
    alert.textContent = message;
    firstCardbody.appendChild(alert);

    //set timeout
    window.setTimeout(() => {
        alert.remove();
    }, 2000);
}

function getTodosLocalStorage() { // storageda todos varsa al yoksa oluştur.
    let todos;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    }else {
        todos = JSON.parse(localStorage.getItem("todos"));
    }
    return todos;
}

function addTodosLocalStorage(newTodo){ // newtodoyu local storagea ekle
    let todos = getTodosLocalStorage();
    todos.push(newTodo);
    localStorage.setItem("todos",JSON.stringify(todos));
}

function addTodosfromStorage(){ // local storagedan todoları ekrana getir.
    let todos = getTodosLocalStorage();

    todos.forEach(todo => {
        addTodotoUI(todo);
    });
}

function deleteTodo(e){ // todo silme
    if (e.target.className === "fa fa-remove") {
        e.target.parentElement.parentElement.remove();
        deleteTodosfromStorage(e.target.parentElement.parentElement.textContent);
        showAlert("success","Başarıyla Silindi!");
    }
}

function deleteTodosfromStorage(deletetodo){ // Local Storagedan da silmek işlemi
    console.log(deleteTodo);
    let todos = getTodosLocalStorage();

    todos.forEach((todo,index) => {
        if (todo === deletetodo) {
            todos.splice(index,1); // o indexte arrayden bir obje sil.
        }
    });

    localStorage.setItem("todos",JSON.stringify(todos));
}

function filterTodos(e){
    
    const filterValue = e.target.value.toLowerCase();
     
    const listItems = document.querySelectorAll(".list-group-item");

    listItems.forEach(listItem =>{
        let text = listItem.textContent.toLowerCase();
        if (text.indexOf(filterValue) === -1) {
            // içinde geçmedi
            listItem.setAttribute("style","display : none !important");
        }else{
            // içinde geçdi
            listItem.setAttribute("style","display : block");
        }
    });
}

function clearAllTodos(){ //tüm todoları silme
    if (confirm("Tümünü silmek istediğinizden emin misiniz ?")) {
        //todoList.innerHTML = ""; // Pahalı DOM methodu

        while (todoList.firstElementChild != null) { // Daha ucuz DOM methodu
            todoList.removeChild(todoList.firstElementChild);
        }
        // localstoragedan da silelim:
        localStorage.removeItem("todos");
    }
}

function todoAlreadyExist(newTodo){
    const inputValue = newTodo.toLowerCase();
    const listItems = document.querySelectorAll(".list-group-item");
    let durum = 0;
    listItems.forEach(listItem =>{
        let text = listItem.textContent.toLowerCase();
        if (text === inputValue) {
            // aynısından var
            durum = 1;
        }
    });
    if (durum === 1) {
        return true;
    }
    else{
        return false;
    }
}